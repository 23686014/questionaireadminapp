<?php

use Illuminate\Database\Seeder;

class QuestionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('questions')->insert([
            ['id' => 1, 'questionnaire_id' => 1, 'title' => "Please rate how easy Visual Studio Code is to import add ons", 'choice' => 1],
            ['id' => 2, 'questionnaire_id' => 1, 'title' => "Please rate the overal usabilty of Visual Studio Code", 'choice' => 2],
            ['id' => 3, 'questionnaire_id' => 1, 'title' => "Please rate how accessible you feel VS Code", 'choice' => 2],
            ['id' => 4, 'questionnaire_id' => 1, 'title' => "VC code has community support", 'choice' => 3],
            ['id' => 5, 'questionnaire_id' => 1, 'title' => "VS code is easy to navigate", 'choice' => 3],
            ['id' => 6, 'questionnaire_id' => 2, 'title' => "which is the best editor", 'choice' => 3],
            


        ]);
        // DB::table('questions')->truncate();
    }
}


