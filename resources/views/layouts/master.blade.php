<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap styles link -->
    <link rel="stylesheet" href="/css/app.css" />
    <title>@yield('title')</title>
</head>

<body>

    <div class="container">
        <!-- Include header.blade.php -->
        <header class="row">
            @include('includes.admin.header')
        </header>

        <article class="row">

            @yield('content')

        </article>
    </div>
    <!-- close container  -->


    <footer class="row">

        @include('includes.footer')

    </footer>


</body>

</html>