@extends('layouts.app')

@section('title', 'Create Questionnaire')

@section('content')
<div class="container">
    <div class="row">
      <h1>Create Questionnaire</h1>

      {!! Form::open(['url' => 'questionnaires']) !!}

    <div class="form-group">
        {!! Form::label('title', 'Title:') !!}
        {!! Form::text('title', null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group">
        {!! Form::label('description', 'Description:') !!}
        {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group">
        {!! Form::submit('Create Questionnaire', ['class' => 'btn btn-primary form-control']) !!}
    </div>


{!! Form::close() !!}

      </article>

    </div>


</div>
@endsection