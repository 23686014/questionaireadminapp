@extends('layouts.app')

@section('title', 'Edit Questionnaire')

@section('content')

<article class="row">
            <h1>Edit - {{ $questionnaire->title }}</h1>

            <!-- include error checking  -->
      

            <!-- form goes here -->
            {!! Form::model($questionnaire, ['method' => 'POST', 'url' => 'questionnaires/', $questionnaire->id]) !!}
     

            <div class="form-group">
        {!! Form::label('title', 'Title:') !!}
        {!! Form::text('title', null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group">
        {!! Form::label('description', 'Description:') !!}
        {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
    </div>

            <div class="form-group">
                {!! Form::submit('Update Questionnaire', ['class' => 'btn btn-primary form-control']) !!}
            </div>
                        
            {!! Form::close() !!}


        </article>
@endsection