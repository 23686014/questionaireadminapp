@extends('layouts.app')

@section('title', 'Questionnaire')

@section('content')
<div class="container">

<div class="panel panel-default">
  <div class="panel-heading"><h1>Title: {{ $questionnaire->title }}</h1></div>
  <div class="panel-body">
    <p class="lead">Instructions: {{ $questionnaire->description }}</p>
    <div class="col-md-12">
      <a href="/questions/create" class="btn btn-lg btn-default pull-right top-buffer">Add Question</a>
      <a href="/questionnaires/{{ $questionnaire->id }}/edit" class="btn btn-default">Edit</a></td>
    </div>
  </div> <!--end of panel body -->
  <table class="table table-striped table-bordered">
        <thead>
            <tr>
                <td>Questions</td>
                <td>Options</td>
                <td>Delete</td>
            </tr>
        </thead>
        <tbody>
            @foreach ($questionnaire->questions as $question)
            <tr>
                <td>Q: {{ $question->title }}</td>
                <td>
                <ol>
                    <li>Strongley Agree</li>
                    <li>Agree</li>
                    <li>Not sure</li>
                    <li>Disagree</li>
                    <li>Strongley Disagree</li>
                </ol>
                </td>
                <td>
                    {!! Form::open(['method' => 'DELETE', 'route' => ['questions.destroy', $question->id]]) !!}
                    {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                    {!! Form::close() !!}
                </td>
            </tr>

            @endforeach
        </tbody>
    </table>

</div>

    
</div>
@endsection