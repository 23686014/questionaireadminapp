@extends('layouts.app')

@section('title', 'My Questionnaires')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-6 card card-header">
            <h1>
            All Questionnaires
            </h1>
            
        </div>
        <div class="col-md-6">
             <a href="questionnaires/create" class="btn btn-lg btn-default pull-right top-buffer">Create Questionnaire</a>
        </div>
        <hr>
    </div>




<section>
    @if (isset ($questionnaires))

    <table class="table table-striped table-bordered">
        <thead>
            <tr>
                <td>Title</td>
                <td>Description</td>
                <td>Published</td>
                <td>View / Edit</td>
                <td>Delete</td>
            </tr>
        </thead>
        <tbody>
            @foreach ($questionnaires as $questionnaire)
            <tr>
                <td>{{ $questionnaire->title }}</td>
                <td>{{ $questionnaire->description }}</td>
                <td>
                    @if($questionnaire->active)
                    Yes
                    @else
                    No
                    @endif
                </td>
                <td> <a href="questionnaires/{{ $questionnaire->id }}" class="btn btn-default">View</a></td>
                <td>
                    {!! Form::open(['method' => 'DELETE', 'route' => ['questionnaires.destroy', $questionnaire->id]]) !!}
                    {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                    {!! Form::close() !!}
                </td>
            </tr>

            @endforeach
        </tbody>
    </table>
    @else
    <p> No Questionnaires added yet </p>
    @endif
</section>
</div>
