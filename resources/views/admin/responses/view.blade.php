@extends('layouts.app')

@section('title', 'My Responses')

@section('content')
<div class="container">
    <div class="row">
      <h1>My Responses</h1>
      <hr>

      <table class="table table-striped table-bordered">
        <thead>
            <tr>
                <td>Questions</td>
                <td>Answer</td>
                <td>Actions</td>
            </tr>
        </thead>
        <tbody>
        
            <tr>
                <td>Q: What is the best code editor</td>
                <td>2. Agree</td>
                <td>
                <button class="btn btn-default">View</button>
                <button class="btn btn-danger">Delete</button>
                </td>
            </tr>


        </tbody>
    </table>

    </div>

</div>
@endsection
