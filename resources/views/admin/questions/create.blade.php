@extends('layouts.app')

@section('title', 'Create Question')

@section('content')
<div class="container">
    <div class="row">
      <h1>Add a Question</h1>

      {!! Form::open(['url' => 'questions']) !!}

    <div class="form-group">
        {!! Form::label('title', 'Question:') !!}
        {!! Form::text('title', null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group">
        {!! Form::label('choice', 'Choices:') !!}
        {!! Form::select('choice', array('1' => 'strongley agree', '2' => 'agree', '3' => 'not sure', '4' => 'disagree', '5' => 'strongley disagree '), null,['placeholder' => 'Select...']) !!}
    </div>

    <div class="form-group">
        {!! Form::submit('Add Question', ['class' => 'btn btn-primary form-control']) !!}
    </div>


{!! Form::close() !!}

      </article>

    </div>


</div>
@endsection