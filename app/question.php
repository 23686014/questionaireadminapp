<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class question extends Model
{
    protected $fillable = [
        'title',
        'choice',
    ];

    public function questionnaire() {
        return $this->belongsTo('App\questionnaire');
      }
}
