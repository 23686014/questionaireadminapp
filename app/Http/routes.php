<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// show a static view for your home page (app/resources/views/home.blade.php)
// Route::get('/', function () {
//     return view('home');
// });

// Resourcefull controller - Home Dashboard page 
Route::resource('/', 'HomeController');

// Resourcefull controller - Questionnaire page
// Route::resource('questionnaires', 'QuestionnaireController');




Route::group(['middleware' => 'web'], function () {
    Route::auth();

    Route::get('/home', 'HomeController@index');
    Route::resource('/questionnaires', 'QuestionnaireController');
    Route::resource('/questions', 'QuestionController');
    Route::resource('/responses', 'QuestionnaireResponsesController');
});