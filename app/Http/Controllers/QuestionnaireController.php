<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\questionnaire;
use App\question;
use App\quesionnaireresponses;

class QuestionnaireController extends Controller
{   

        /*
    * Secure the set of pages to the admin.
    */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // get all the questionnaires
        $questionnaires = questionnaire::all();
        return view('admin.questionnaires.overview')->with('questionnaires', $questionnaires);
       
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.questionnaires.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        questionnaire::create($input);
    
        return redirect('questionnaires');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function show($id)
    {
        $questionnaire = questionnaire::findOrFail($id);
        
    
        return view('admin.questionnaires.show', compact('questionnaire'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $questionnaire = questionnaire::findOrFail($id);

        return view('admin.questionnaires.edit', compact('questionnaire'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $questionnaire = questionnaire::findOrFail($id);

        $questionnaire->update($request->all());

        return redirect('questionnaires', compact('questionnaire') );

        // return redirect()->action('QuestionnaireController@show');

        

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $questionnaire = questionnaire::find($id);

        $questionnaire->delete();

        return redirect('questionnaires');
    }
}
