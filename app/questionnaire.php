<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class questionnaire extends Model
{
        
    protected $fillable = [
        'title',
        'description',
        'active',
    ];


    public function questions() {
        return $this->hasMany('App\question');
    }

    public function answers() {
        return $this->hasMany('App\questionnaireresponses');
    }
}
