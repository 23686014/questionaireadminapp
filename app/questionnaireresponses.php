<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class questionnaireresponses extends Model
{
    protected $fillable = [
        'answer',
    ];

    public function questionnaire() {
        return $this->belongsTo('App\questionnaire');
      }
}
